<?php

$routes = array(
    '/' => array('App\Controller\DefaultController', 'indexAction')
);

foreach ($routes as $uri => $route) {
    if ($uri === $_SERVER['REQUEST_URI']) {
        $controller = new $route[0];
        $controller->{$route[1]}();
    }
}