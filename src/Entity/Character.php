<?php

namespace App\Entity;

class Character extends AbstractModel
{
    /**
     * @var string
     */
    private $account;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $inventory;

    public function __construct()
    {
        $this->setTableName('Character');
        $this->setColumnMetadata([
            'account' => [
                'column' => 'AccountID',
                'type' => 'string'
            ],
            'name' => [
                'column' => 'Name',
                'type' => 'string'
            ],
            'inventory' => [
                'column' => 'Inventory',
                'type' => 'varbinary',
                'length' => 1520
            ],
        ]);
    }

    /**
     * @return string
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * @param string $account
     */
    public function setAccount($account)
    {
        $this->account = $account;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getInventory()
    {
        return $this->inventory;
    }

    /**
     * @param string $inventory
     */
    public function setInventory($inventory)
    {
        $this->inventory = $inventory;
    }
}