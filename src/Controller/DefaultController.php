<?php

namespace App\Controller;

use App\Entity\Character;
use App\Entity\Warehouse;
use App\Model\Duplicate;

class DefaultController extends AbstractController
{
    public function indexAction()
    {
        $db = $this->getDb();

        $warehouses = $db->findAll(Warehouse::class);
        $characters = $db->findAll(Character::class);

        $allItemsInServerOnlyHex = [];
        $allItemsInServerWithAccountName = [];

        /** @var Warehouse $warehouse */
        foreach ($warehouses as $warehouse) {
            $warehouseItems = str_split($warehouse->getItems(), 20);
            foreach ($warehouseItems as $warehouseItem) {
                if ($warehouseItem !== 'FFFFFFFFFFFFFFFFFFFF') {
                    $allItemsInServerOnlyHex[] = substr($warehouseItem, 6, 8);
                    $allItemsInServerWithAccountName[] = [
                        'hex' => $warehouseItem,
                        'serial' => substr($warehouseItem, 6, 8),
                        'accountName' => $warehouse->getAccount(),
                        'characterName' => null,
                        'from' => 'Warehouse'
                    ];
                }
            }
        }

        /** @var Character $character */
        foreach ($characters as $character) {
            $inventoryItems = str_split($character->getInventory(), 20);
            foreach ($inventoryItems as $inventoryItem) {
                if ($inventoryItem !== 'FFFFFFFFFFFFFFFFFFFF') {
                    $allItemsInServerOnlyHex[] = substr($inventoryItem, 6, 8);
                    $allItemsInServerWithAccountName[] = [
                        'hex' => $inventoryItem,
                        'serial' => substr($inventoryItem, 6, 8),
                        'accountName' => $character->getAccount(),
                        'characterName' => $character->getName(),
                        'from' => 'Inventory'
                    ];
                }
            }
        }

        $itemsCountValues = array_count_values($allItemsInServerOnlyHex);

        $duplicates = [];
        foreach ($itemsCountValues as $key => $itemsCountValue) {
            if ($itemsCountValue > 1) {
                if ($key !== "00000000") {
                    $duplicates[] = [
                        'serial' => $key,
                        'count' => $itemsCountValue
                    ];
                }
            }
        }

        $duplicatesWithAccountName = [];
        foreach ($duplicates as $duplicate) {
            $duplicatesWithAccountName[] = array_filter($allItemsInServerWithAccountName, function ($item) use ($duplicate) {
                return $item['serial'] === $duplicate['serial'];
            }, ARRAY_FILTER_USE_BOTH);
        }

        $allDuplicates = [];
        foreach (reset($duplicatesWithAccountName) as $duplicate) {
            $duplicateModel = new Duplicate();
            $duplicateModel->setAccount($duplicate['accountName']);
            $duplicateModel->setCharacter($duplicate['characterName'] === null ? 'N/A' : $duplicate['characterName']);
            $duplicateModel->setItem($duplicate['hex']);
            $duplicateModel->setSerial($duplicate['serial']);
            $duplicateModel->setLocation($duplicate['from']);
            $allDuplicates[] = $duplicateModel;
        }

        include 'src/view/index.php';
    }
}