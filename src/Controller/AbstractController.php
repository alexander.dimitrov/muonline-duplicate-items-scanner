<?php

namespace App\Controller;

use App\Service\Database;

abstract class AbstractController
{
    public function getDb()
    {
        return new Database();
    }
}