<?php

namespace App\Service;

class Database
{
    /**
     * @var \PDO
     */
    private $connection;

    public function __construct()
    {
        try {
            $config = require_once 'config/config.php';
            $this->connection = new \PDO('sqlsrv:Server=' . $config['hostname'] . ',1433;Database=' . $config['db'], $config['user'], $config['pass']);
        } catch (\PDOException $exception) {
            echo 'Failed connect to database: ' . $exception->getMessage();
        }
    }

    public function getConnection()
    {
        return $this->connection;
    }

    public function findOneBy($entityClass, $params = [])
    {
        $entity = new $entityClass;

        $sql = 'SELECT';

        $metadataCount = count($entity->getColumnMetadata());
        $selectIteration = 1;
        foreach ($entity->getColumnMetadata() as $columnParamName => $metadata) {
            if ($metadata['type'] === 'varbinary') {
                $sql .= ' convert(VARCHAR(' . $metadata['length'] . '), ' . $metadata['column'] . ', 2) as ' . $metadata['column'];
            } else {
                $sql .= ' ' . $metadata['column'] . ' as ' . $metadata['column'];
            }
            if ($metadataCount > 1 && $selectIteration !== $metadataCount) {
                $sql .= ', ';
            }
            $selectIteration++;
        }

        $sql .= ' FROM ' . $entity->getTable() . ' WHERE ';

        $whereIteration = 1;
        foreach ($params as $paramName => $param) {
            foreach ($entity->getColumnMetadata() as $columnParamName => $metadata) {
                if ($paramName === $columnParamName) {
                    if ($whereIteration === 1) {
                        $sql .= $metadata['column'] . ' = \'' . $param . '\'';
                    } else {
                        $sql .= 'AND ' . $metadata['column'] . ' = \'' . $param . '\'';
                    }
                }
            }
            $whereIteration++;
        }

        $stmt = $this->getConnection()->query($sql);
        if ($stmt) {
            $result = $stmt->fetch();

            if ($result) {
                foreach ($entity->getColumnMetadata() as $key => $metadata) {
                    $entity->{'set' . ucwords($key)}($result[$metadata['column']]);
                }
            } else {
                throw new \Exception('Entity not found', 404);
            }

            return $entity;
        } else {
            return null;
        }
    }

    public function findAll($entityClass)
    {
        $metadataInstance = new $entityClass;

        $sql = 'SELECT';

        $metadataCount = count($metadataInstance->getColumnMetadata());
        $selectIteration = 1;
        foreach ($metadataInstance->getColumnMetadata() as $columnParamName => $metadata) {
            if ($metadata['type'] === 'varbinary') {
                $sql .= ' convert(VARCHAR(' . $metadata['length'] . '), ' . $metadata['column'] . ', 2) as ' . $metadata['column'];
            } else {
                $sql .= ' ' . $metadata['column'] . ' as ' . $metadata['column'];
            }
            if ($metadataCount > 1 && $selectIteration !== $metadataCount) {
                $sql .= ', ';
            }
            $selectIteration++;
        }

        $sql .= ' FROM ' . $metadataInstance->getTable();
        $stmt = $this->getConnection()->query($sql);

        if ($stmt) {
            $results = $stmt->fetchAll();
            $return = [];
            if ($results) {
                foreach ($results as $result) {
                    $entity = new $entityClass;
                    foreach ($metadataInstance->getColumnMetadata() as $key => $metadata) {
                        $entity->{'set' . ucwords($key)}($result[$metadata['column']]);
                    }
                    $return[] = $entity;
                }
            }
            return $return;
        }
        return [];
    }
}