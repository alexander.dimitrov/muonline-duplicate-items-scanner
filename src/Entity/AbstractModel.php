<?php

namespace App\Entity;

abstract class AbstractModel
{
    /**
     * @var string
     */
    private $tableName;

    /**
     * @var array
     */
    private $columnMetadata;

    protected function setColumnMetadata($metadata)
    {
        $this->columnMetadata = $metadata;
    }

    protected function setTableName($tableName)
    {
        $this->tableName = $tableName;
    }

    public function getColumnMetadata()
    {
        return $this->columnMetadata;
    }

    public function getTable()
    {
        return $this->tableName;
    }
}