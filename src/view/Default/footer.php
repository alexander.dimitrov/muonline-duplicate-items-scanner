</main>

<footer>
    <div class="fixed-bottom bg-dark text-white p-3 text-center">
        Created by: <a href="mailto:alexandar.dimitrov11@gmail.com">
            <img src="../../../assets/img/developer-logo.png" alt="Alexander Dimitrov"/>
        </a>
    </div>
</footer>


<script src="../../../assets/js/jquery-3.5.1.slim.min.js" crossorigin="anonymous"></script>
<script src="../../../assets/js/bootstrap-4.5.4.bundle.min.js" crossorigin="anonymous"></script>

</body>
</html>