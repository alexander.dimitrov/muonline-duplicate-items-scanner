<div class=" container jumbotron mt-3">
    <h1>Duplicated items</h1>
    <hr/>
    <table class="table table-striped">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Account</th>
            <th scope="col">Character</th>
            <th scope="col">Item</th>
            <th scope="col">Serial</th>
            <th scope="col">Location</th>
        </tr>
        </thead>
        <tbody>
        <?php
        foreach ($allDuplicates as $key => $item) {
            echo '<tr>';
            echo '<td>' . ($key + 1) . '</td>';
            echo '<td>' . $item->getAccount() . '</td>';
            echo '<td>' . $item->getCharacter() . '</td>';
            echo '<td>' . $item->getItem() . '</td>';
            echo '<td>' . $item->getSerial() . '</td>';
            echo '<td>' . $item->getLocation() . '</td>';
            echo '</tr>';
        }

        ?>

        </tbody>
    </table>
</div>