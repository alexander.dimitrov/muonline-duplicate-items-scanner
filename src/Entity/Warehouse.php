<?php

namespace App\Entity;


class Warehouse extends AbstractModel
{
    /**
     * @var string
     */
    private $account;

    /**
     * @var string
     */
    private $items;

    public function __construct()
    {
        $this->setTableName('Warehouse');
        $this->setColumnMetadata([
            'account' => [
                'column' => 'AccountID',
                'type' => 'string'
            ],
            'items' => [
                'column' => 'Items',
                'type' => 'varbinary',
                'length' => 2400
            ],
        ]);
    }

    /**
     * @return string
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * @param string $account
     */
    public function setAccount($account)
    {
        $this->account = $account;
    }

    /**
     * @return string
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @param string $items
     */
    public function setItems($items)
    {
        $this->items = $items;
    }


}